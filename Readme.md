# Littlefork media plugin

Operations on media files and URL's.

## Plugins

- `media_exif`

Extract exif data from image urls in `_lf_media` fields.

`littlefork -c config.json -q queries.json -p google_images,media_exif`
