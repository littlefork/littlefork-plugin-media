import exifPlugin from './plugins/exif';

const plugins = {
  media_exif: exifPlugin,
};

export { plugins };
export default { plugins };
